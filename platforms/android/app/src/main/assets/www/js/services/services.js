angular
    .module('trackrApp')
    .factory('Utils', function() {
      function getStatus(status){
        // 1-Open 2-Accepted 3-Cancelled 4-Payment Required 5-Paid 6-Rejected 7-Expired
        switch(status) {
            case 1:
                return "Open";
                break;
            case 2:
                return "Accepted";
                break;
            case 3:
                return "Cancelled";
                break;
            case 4:
                return "Payment Required";
                break;
            case 5:
                return "Paid";
                break;
            case 6:
                return "Rejected";
                break;
            case 7:
                return "Expired";
                break;
        }
      }

      function getType(type){
        switch(type) {
            case 1:
                return "User";
                break;
            case 2:
                return "Professional";
                break;
        }
      }

      return {
        getJobFormatted: function(job){
          if(moment(new Date()).isAfter(job.scheduled_date)){
            job.status = 7;
          }
          var hour = job.start_hour.toString().substring(0, 2);
          var min = job.start_hour.toString().substring(2, 4);
          var start_hour_formatted = new Date('1999','01','01',hour,min);
          var end_hour_formatted = moment(new Date('1999','01','01',hour,min)).add(job.estimated_hours, 'hours');
          job.start_hour = start_hour_formatted.getTime();
          job.end_hour = end_hour_formatted.valueOf();
          job.curr_status = getStatus(job.status);
          return job;
        },
        getBalanceFormatted: function(jobs){
          var paid = jobs.filter(function (el) {
            return el.status == 5;
          });

          var formattedJobs = paid.map(function(job){
            job.week = moment(job.scheduled_date, "YYYY-MM-DD HH:mm:ss").isoWeek();
            job.firstDayOfWeek = moment().day("Monday").week(job.week);
            job.lastDayOfWeek = moment().day("Saturday").week(job.week);
            return job;
          });

          return formattedJobs.sort(function(a, b){return a.week-b.week});
        },
        getUserFormatted: function(user){
          user.utype = getType(user.type);
          return user;
        }
      }
    })
    .factory('Admin', function($http) {
        return {
            getUsers: function() {
                return $http({
                    url: __env.apiUrl + '/users',
                    method: "GET"
                });
            }
        }
    })
    .factory('Users', function($http) {
        return {
            getUser: function(userId) {
                return $http({
                    url: __env.apiUrl + '/users/' + userId,
                    method: "GET"
                });
            }
        }
    })
    .factory('Companies', function($http) {
        return {
            getCompany: function() {
                return $http({
                    url: __env.apiUrl + '/companies',
                    method: "GET"
                });
            }
        }
    })
    .factory('Professionals', function($http) {
        return {
            getProfessionals: function() {
                return $http({
                    url: __env.apiUrl + '/professionals',
                    method: "GET"
                });
            },
            getProfessional: function(professionalId) {
                return $http({
                    url: __env.apiUrl + '/users/' + professionalId,
                    method: "GET"
                });
            }
        }
    })
    .factory('Company', function($http) {
        return {
            getFare: function() {
                return $http({
                    url: __env.apiUrl + '/companies',
                    method: "GET"
                });
            }
        }
    })
    .factory('Jobs', function($http) {
        return {
            getJobs: function(userId) {
                return $http({
                    url: __env.apiUrl + '/jobs/'+ userId,
                    method: "GET"
                });
            },
            getJob: function(jobId) {
                return $http({
                    url: __env.apiUrl + '/job/'+ jobId,
                    method: "GET"
                });
            },
            acceptJob: function(jobId, professionalId) {
                return $http({
                    url: __env.apiUrl + '/jobs/'+ jobId,
                    method: "PUT",
                    data:{professional: professionalId, status: 2}
                });
            },
            requestPayment: function(jobId, professionalId) {
                return $http({
                    url: __env.apiUrl + '/jobs/'+ jobId,
                    method: "PUT",
                    data:{professional: professionalId, status: 4}
                });
            },
            paymentDone: function(jobId) {
                return $http({
                    url: __env.apiUrl + '/jobs/'+ jobId,
                    method: "PUT",
                    data:{status: 5}
                });
            },
            cancelJob: function(jobId) {
                return $http({
                    url: __env.apiUrl + '/jobs/'+ jobId,
                    method: "PUT",
                    data:{status: 3}
                });
            },
            visualizedJobUser: function(jobId) {
                return $http({
                    url: __env.apiUrl + '/jobs/'+ jobId,
                    method: "PUT",
                    data:{user_notification: 1}
                });
            },
            visualizedJobProfessional: function(jobId) {
                return $http({
                    url: __env.apiUrl + '/jobs/'+ jobId,
                    method: "PUT",
                    data:{professional_notification: 1}
                });
            },
            getUnreadUser: function(userId) {
                return $http({
                    url: __env.apiUrl + '/jobs/'+ userId,
                    method: "GET"
                }).then(function(response){
                  var unread = 0;
                  return response.data.map(function(job){
                    if(job.user_notification !== undefined && job.user_notification === 0){
                      unread += 1;
                    }
                    return unread;
                  });
                });
            },
            getUnreadProfessional: function(userId) {
                return $http({
                    url: __env.apiUrl + '/jobs/'+ userId,
                    method: "GET"
                }).then(function(response){
                  var unread = 0;
                  return response.data.map(function(job){
                    if(job.professional_notification !== undefined && job.professional_notification === 0){
                      unread += 1;
                    }
                    return unread;
                  });
                });
            },
            getJobsFeed: function() {
                return $http({
                    url: __env.apiUrl + '/jobs',
                    method: "GET",
                    data: { status: 1}
                });
            }
        }
    })
  .service('jobService', function() {
    var jobService = this;
    jobService.sharedObject = {};

    jobService.getJob = function(){
       return jobService.sharedObject.Job;
    }

    jobService.setJob = function(value){
       jobService.sharedObject.Job = value;
    }
  });
