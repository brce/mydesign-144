angular.module('mydesignApp')
.controller('shapeCtrl', ['$window', '$rootScope', '$scope', '$http', '$location','md5', function ($window, $rootScope, $scope, $http, $location, md5) {
    $scope.back = function() { 
        window.history.back();
     }; 
     
     $scope.logout = function () {
        $window.localStorage.removeItem('userInfo');
        $location.path('home');
    };
    
    if ($location.path() === '/chooseshape') {
        $('.loading').fadeIn(300);
        $rootScope.img_path = __env.apiUrl + '/../static/';  
        $http.get(__env.apiUrl + '/bodyshapes')
        .success(function(data, status, headers, config) {
            $scope.shapes = data.sort(function(a, b){
                return a.order > b.order;
            });
            $('.loading').fadeOut(300);
        })
        .error(function(data, status, headers, config) {
            $scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
        });
    }
    
    $scope.confirmShape = function(shape){
        $rootScope.shape = shape;
        $location.path('/confirmshape');
    };

    $scope.saveShape = function(shape){
        if ($rootScope.user === null || $rootScope.user === undefined || !$rootScope.user.logged){
            $rootScope.user = {"id": md5.createHash(Date.now)}
            $rootScope.user.logged = false;
        }
        $http.post( __env.apiUrl + '/bodyshape/'+ shape.id +'/user/' + $rootScope.user.id)
        .success(function(data, status, headers, config) {
            $location.path('/choosecategory');
            $rootScope.user.shape = shape.order;
        })
        .error(function(data, status, headers, config) {
            $scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
        });
    };
}]);
