angular.module('mydesignApp')
.controller('paymentController', ['$window', '$rootScope', '$scope','$http', '$location',
  function ($window, $rootScope, $scope, $http, $location) {
  $scope.back = function() { 
      window.history.back();
  };  

  $scope.logout = function () {
    $window.localStorage.removeItem('userInfo');
    $location.path('home');
  };

  $scope.country = [
    { code: "canada", name: "Canada" },
    { code: "usa", name: "United States" }
  ];

  $scope.province = [
    { code: "AB", name: "Alberta" },
    { code: "BC", name: "British Columbia" },
    { code: "MB", name: "Manitoba" },
    { code: "NB", name: "New Brunswick" },
    { code: "NL", name: "Newfoundland and Labrador" },
    { code: "NS", name: "Nova Scotia" },
    { code: "ON", name: "Ontario" },
    { code: "PE", name: "Prince Edward Island" },
    { code: "QC", name: "Quebec" },
    { code: "SK", name: "Saskatchewan" },
    { code: "NT", name: "Northwest Territories" },
    { code: "NU", name: "Nunavut" },
    { code: "YT", name: "Yukon" }
  ];

  $scope.state = [
    { code: "AL", name: "Alabama" },
    { code: "AK", name: "Alaska" },
    { code: "AZ", name: "Arizona" },
    { code: "AR", name: "Arkansas" },
    { code: "CA", name: "California" },
    { code: "CO", name: "Colorado" },
    { code: "CT", name: "Connecticut" },
    { code: "DE", name: "Delaware" },
    { code: "DC", name: "District Of Columbia" },
    { code: "FL", name: "Florida" },
    { code: "GA", name: "Georgia" },
    { code: "HI", name: "Hawaii" },
    { code: "ID", name: "Idaho" },
    { code: "IL", name: "Illinois" },
    { code: "IN", name: "Indiana" },
    { code: "IA", name: "Iowa" },
    { code: "KS", name: "Kansas" },
    { code: "KY", name: "Kentucky" },
    { code: "LA", name: "Louisiana" },
    { code: "ME", name: "Maine" },
    { code: "MD", name: "Maryland" },
    { code: "MA", name: "Massachusetts" },
    { code: "MI", name: "Michigan" },
    { code: "MN", name: "Minnesota" },
    { code: "MS", name: "Mississippi" },
    { code: "MO", name: "Missouri" },
    { code: "MT", name: "Montana" },
    { code: "NE", name: "Nebraska" },
    { code: "NV", name: "Nevada" },
    { code: "NH", name: "New Hampshire" },
    { code: "NJ", name: "New Jersey" },
    { code: "NM", name: "New Mexico" },
    { code: "NY", name: "New York" },
    { code: "NC", name: "North Carolina" },
    { code: "ND", name: "North Dakota" },
    { code: "OH", name: "Ohio" },
    { code: "OK", name: "Oklahoma" },
    { code: "OR", name: "Oregon" },
    { code: "PA", name: "Pennsylvania" },
    { code: "RI", name: "Rhode Island" },
    { code: "SC", name: "South Carolina" },
    { code: "SD", name: "South Dakota" },
    { code: "TN", name: "Tennessee" },
    { code: "TX", name: "Texas" },
    { code: "UT", name: "Utah" },
    { code: "VT", name: "Vermont" },
    { code: "VA", name: "Virginia" },
    { code: "WA", name: "Washington" },
    { code: "WV", name: "West Virginia" },
    { code: "WI", name: "Wisconsin" },
    { code: "WY", name: "Wyoming" }
  ];

  if ($location.path() === '/cart' && (
    $rootScope.cart === undefined || $rootScope.cart.length === 0)) {
      $scope.showError = true;
      $scope.message = 'Your cart is empty!';
      $('#confirmation-window').show(); 
  }

  if ($location.path() === '/deliveryaddress' && $rootScope.address === undefined 
        && $rootScope.user !== undefined && /^\d+$/.test($rootScope.user.id)) {
    $http.get( __env.apiUrl + '/address/'+ $rootScope.user.id)
    .success(function(data, status, headers, config){
      $rootScope.address = data;
    })
    .error(function(data, status, headers, config) {
      $scope.showError = true;
      $scope.message = 'Oops! Something went wrong!';
      $('#confirmation-window').show(); 
    });
  }  

  function saveAddresses(address) {
      var endpoint = '/address'
      if($rootScope.address !== undefined && $rootScope.address.id !== null 
            && $rootScope.address.id !== undefined) {
        endpoint = '/edit_address'
      }
      address.user_id = $rootScope.user.id;
      return $http.post(__env.apiUrl + endpoint, address);
  }

  function getShippingPrice() {
    switch($rootScope.billing.province){
      case "AB":
        $rootScope.hst = 0.05;
      break;
      case "BC":
      $rootScope.hst = 0.12;
      break;
      case "MB":
      $rootScope.hst = 0.13;
      break;
      case "NB":
      $rootScope.hst = 0.15;
      break;
      case "NL":
      $rootScope.hst = 0.15;
      break;
      case "NS":
      $rootScope.hst = 0.05;
      break;
      case "NT":
      $rootScope.hst = 0.15;
      break;
      case "NU":
      $rootScope.hst = 0.05;
      break;
      case "ON":
      $rootScope.hst = 0.13;
      break;
      case "PE":
      $rootScope.hst = 0.15;
      break;
      case "QC":
      $rootScope.hst = 0.1498;
      break;
      case "SK":
      $rootScope.hst = 0.11;
      break;
      case "YT":
      $rootScope.hst = 0.05;
      break;
      
      default: 
        $rootScope.hst = 0.15;
      break;
    }
  }

  $scope.saveAddress = function(address, billing) {
    $('.loading').fadeIn(300);
    address.is_billing = false;
    address.country = 'Canada';
    $rootScope.address = address;
    var addr = saveAddresses(address);

    if (billing !== undefined && billing !== null) {
      addr.then(function(){
          billing.is_billing = true;
          billing.country = 'Canada';
          $rootScope.billing = billing;
          saveAddresses(billing)
          .success(function(data, status, headers, config) {
            $('.loading').fadeOut(300);
            getShippingPrice();
            $location.path('/shippingtype');
          })
          .error(function(data, status, headers, config) {
            $('.loading').fadeOut(300);
            $scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
          });
      });
    } else {
      billing = address;
      $rootScope.billing = address;
      addr.then(function(){
          billing.is_billing = true;
          saveAddresses(billing)
          .success(function(data, status, headers, config) {
            $('.loading').fadeOut(300);
            getShippingPrice();
            $location.path('/shippingtype');
          })
          .error(function(data, status, headers, config) {
            $('.loading').fadeOut(300);
            $scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
          });
      });
    }
  }

  $scope.shippingType = function(shippingType) {
    $rootScope.shippingType = shippingType;
    if (shippingType === 'standard') {
      $rootScope.shippingDescription = 'Standart Shipping';
      $rootScope.shippingPrice = 14.95;
    } else {
      $rootScope.shippingDescription = 'Fast Shipping';
      $rootScope.shippingPrice = 35.00;
    }
    $location.path('/giftoptions');
  }

  if ($location.path() === '/giftoptions' && $rootScope.giftOptions === undefined) {
    $rootScope.giftOptions = { notAGift: true };
  }

  $scope.gift = function(giftOptions) {
    $rootScope.giftOptions = giftOptions;
    $rootScope.wrappingPrice = 0;
    $location.path('/paymentreview');
  }

  $scope.payment = function() {
    $location.path('/payment');
  }

  $scope.delete = function(item){
    $rootScope.cart.total = $rootScope.cart.total - (item.price + item.fprice);
    var index = $rootScope.cart.items.indexOf(item);
    $rootScope.cart.items.splice(index, 1);      
  };
  
  $scope.charge = function() {
    $('.loading').fadeIn(300);
      Stripe.card.createToken({
        number: $scope.payment.card,
        cvc: $scope.payment.cvs,
        exp_month: $scope.payment.exp_month,
        exp_year: $scope.payment.exp_year
      }, function stripeResponseHandler(status, response) {
        if (response.error) {
          $('.loading').hide();
          $scope.showError = true;
          $scope.message = response.error.message.substring(0, 30) + '...';
          $('#confirmation-window').show(); 
        }else{
          var purchase = {}
          purchase.token = response.id
          purchase.amount = (($rootScope.cart.total + $rootScope.shippingPrice)*(1+$rootScope.hst)).toFixed(2);
          purchase.order_id = $rootScope.cart.id;
          purchase.shipping_price = $rootScope.shippingPrice;
          purchase.is_gift = !$rootScope.giftOptions.notAGift;
          purchase.wrapping_price = $rootScope.wrappingPrice;
          purchase.wrapping = $rootScope.giftOptions.wrapping;
          purchase.is_send_receipt = $rootScope.giftOptions.giftReceipt;
          purchase.message = $rootScope.giftOptions.message;
          purchase.email = $rootScope.user.logged ? $rootScope.user.email : $scope.payment.email;
          $http.post(__env.apiUrl + '/payments', purchase)
          .success(function(data, status, headers, config) {
            $('.loading').fadeOut(300);
            $rootScope.orderNumber = JSON.parse(data.payload).id;
            $rootScope.cart = [];
            $http.post(__env.apiUrl + '/sendreceipt', { cart_id: $rootScope.cart.id })
            .success(function(data, status, headers, config) {
              $location.path('/paymentdone');
            }).error(function(data, status, headers, config) {
              $('.loading').fadeOut(300);
              $scope.showError = true;
              $scope.message = 'Oops! Something went wrong!';
              $('#confirmation-window').show(); 
            });
          })
          .error(function(data, status, headers, config) {
            $('.loading').fadeOut(300);
            $scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
          });
        }
      });
    }
  }])