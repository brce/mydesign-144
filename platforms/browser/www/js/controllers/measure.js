angular.module('mydesignApp')
.controller('measureCtrl', ['$window', '$rootScope', '$scope', '$http', '$location', function ($window, $rootScope, $scope, $http, $location) {  

    $scope.back = function() { 
        window.history.back();
    };

    $scope.logout = function () {
        $window.localStorage.removeItem('userInfo');
        $location.path('home');
    };

    $scope.redirect = function (page) {
        $rootScope.page = page;
        var alturaTela = $(document).height();
        var larguraTela = $(window).width();


        $('#confirmation-mask').css({
            'width': larguraTela,
            'height': alturaTela
        });
        $('#confirmation-mask').fadeIn(1000);
        $('#confirmation-mask').fadeTo("slow", 0.8);

        $('#confirmation-window').show();
    };

    if ($location.path() === '/sizechart') {
        $scope.title = "What's your size?";
        $scope.clothe = $rootScope.clothes;
        $scope.visible = false;
    } else if ($location.path() === '/confirmsize') {
        $scope.title = "Is this your size?";
        $scope.visible = true;
    }
    
    $scope.chooseMeasures = function(measure){
        $rootScope.measure = measure;
        $location.path('/confirmfabric');
    };  
    
    $scope.confirmSize = function(size){
        $rootScope.model.chest = $scope.clothes['chest_' + size.toLowerCase()];
        $rootScope.model.hip = $scope.clothes['hip_' + size.toLowerCase()];
        $rootScope.model.waist = $scope.clothes['waist_' + size.toLowerCase()];
        $rootScope.model.length = $scope.clothes['length_' + size.toLowerCase()];
        $location.path('/adjustsize');
    };

    $scope.confirmAdjustSize = function(){
        if ($rootScope.model.chest === null) {
            $scope.msg = true;
            $scope.message = 'Choose your measure before proceed!';
            $('#confirmation-window').show(); 
        } else {
            $location.path('/confirmadjust');
        }
    };

    function add_to_cart(){
        var itemObj = {
            "id": $rootScope.model.id,
            "user_id": $rootScope.user.id,
            "chest": $rootScope.model.chest,
            "waist": $rootScope.model.waist,
            "hip": $rootScope.model.hip,
            "length": $rootScope.model.length,
          };
  
        $http.post( __env.apiUrl + '/add_to_cart',  itemObj)
        .success(function(data, status, headers, config) {
            $rootScope.img_path = __env.apiUrl + '/../static/';
            $scope.item = JSON.parse(data.payload);
            if ($rootScope.cart === undefined || $rootScope.cart.id === undefined) {
                $rootScope.cart = {};
                $rootScope.cart.id = $scope.item.cart_id;
                $rootScope.cart.items = [];
                $rootScope.cart.total = 0;
            }
            $rootScope.cart.items.push($scope.item);
            $rootScope.cart.total += $scope.item.price + $scope.item.fprice;
        })
        .error(function(data, status, headers, config) {
			$scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
		});
    }

    function add_to_closet(){
        var itemObj = {
            "user_id": $rootScope.user.id,
            "item_id": $scope.model.id
          };
  
        $http.post(__env.apiUrl + '/add_to_closet', itemObj)
        .success(function(data, status, headers, config) {
            $location.path('/checkout');
        })
        .error(function(data, status, headers, config) {
			$scope.showError = true;
            $scope.message = 'Oops! Something went wrong!';
            $('#confirmation-window').show(); 
		});
    }

    $scope.addToCart = function(){
        add_to_cart();
        $location.path('/checkout');
    };

    $scope.saveToCloset = function(){
        add_to_cart();
        add_to_closet();
    };

    $scope.checkout = function(){
        if ($rootScope.user.logged) {
            $location.path('/deliveryaddress');
        }
        $location.path('/checkoutlogin');
    };

    $scope.continueShopping = function(){
        $location.path('/choosecategory');
    };

}]);
